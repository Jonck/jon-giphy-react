import React from 'react';
import Gifs from './components/gifs'
import {SingleGif} from './components/singleGif'
import { Route } from "wouter";
import './App.css';
import {GrfsContextProvider} from './context/gifContext'


function App() {
    return (
        <div className="App">
            <section className="App-content"> 
            <GrfsContextProvider>   
                <Route 
                    path={'/'}
                    component={Gifs} />     
                <Route 
                    path={'/find/:keyword'}
                    component={Gifs} />
                <Route 
                    path={'/findid/:id'}
                    component={SingleGif} />
            </GrfsContextProvider>
            </section>
        </div>
    );  
}

export default App;
