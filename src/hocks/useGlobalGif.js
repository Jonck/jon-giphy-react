import {useContext} from 'react';
import {GifContext} from 'context/gifContext';

export const useGlobalGif = () => {
    const {gifs} = useContext(GifContext); 
    return gifs;
}