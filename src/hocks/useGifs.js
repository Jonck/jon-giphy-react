import {useEffect, useContext} from 'react';
import {GifContext} from 'context/gifContext';

export function useGifs ({keyParam}) {
    const {gifs, setGifs} = useContext(GifContext);

    const getGifs = (keyword, value) => {
        keyword = keyword && keyword !== '' ? keyword : 'javaScript';
        value = value && value !== '' ? value : 10;

        fetch(`https://api.giphy.com/v1/gifs/search?api_key=BWe7OJcqiVGCBoyCo5MbeZsUt1JherH7&q=${keyword}&limit=${value}&offset=0&rating=g&lang=en`)
            .then(res => res.json())
            .then(res => setGifs(res.data))
            .catch(error => console.log(error));
    }

    const getKeyword = e => {
        const parent = e.target.parentNode,
          inputKeyword = parent.querySelector('[type="text"]').value.trim().toLowerCase().split(' ').join('%20'),
          inputNumber =  parent.querySelector('[type="number"]').value;
        getGifs(inputKeyword, inputNumber)
      };

    useEffect(() => {
        getGifs(keyParam);
    }, [keyParam])

    return {gifs, setGifs, getKeyword, getGifs}
}

