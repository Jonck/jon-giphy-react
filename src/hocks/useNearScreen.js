import {useEffect, useState} from 'react';


export const useIntersectionObserver = ({elementRef}) => {
    const [isNearScreen, setShow] = useState(false);

    useEffect(() => {
        const onChage = (entries, observer) => {
            if(entries[0].isIntersecting){
                setShow(true);
                observer.disconnect();
            };
        }

        const observer = new IntersectionObserver(onChage, {
            rootMargin: '50px'
        });

        observer.observe(elementRef.current);
    }, [elementRef]);

    return isNearScreen;
}