import React from 'react';
import {InterPrint} from './print/gif';
import {useGlobalGif} from 'hocks/useGlobalGif';

export const SingleGif = props => {
    const {params:{id:keyId}} = props,
        gifs = useGlobalGif(),
        dataGif = gifs.filter(item => item.id === keyId ? item : '');

    return (
        <div className='App-gifs'>
            {dataGif.map(gif => {
                const {id, title, images:{preview_gif:image}} = gif;
                return (
                    <InterPrint 
                        id={id}
                        urlLink={'/'}
                        title={title}
                        urlImg={image.url}
                        key={id}
                    />
                )
            })}
        </div>
    )
}