import React from 'react';
import {InterPrint} from 'components/print/gif';
import {useGifs} from 'hocks/useGifs';

const Gifs = props => {
    const keyParam = props.params.keyword,
        {gifs, getKeyword, getGifs} = useGifs({keyParam});
  
    return (
        <> 
            <form action='GET' className='App-form'>
                <input 
                    placeholder={keyParam ? keyParam : 'javaScript' } 
                    className='App-input' 
                    type='text' 
                    onChange={getKeyword} 
                />
                <input 
                    placeholder='1' 
                    className='App-input' 
                    type='number' 
                    onChange={getKeyword} 
                    max="100" 
                    min="1"
                />
            </form>
            <button 
                className='App-buttom' 
                onClick={() => getGifs(keyParam !== '' ? keyParam : 'javaScript' )}> 

                    Default 
            
            </button>
            <div className='App-gifs'>
                {gifs.map(gif => {
                    const {id, title, images:{preview_gif:image}} = gif;
                    return (
                        <InterPrint 
                            id={id}
                            urlLink={'/findid/'+id}
                            title={title}
                            urlImg={image.url}
                            key={id}
                        />
                    )
                })}
            </div>
        </>
    );
}

export default Gifs;