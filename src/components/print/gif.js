import React, {useRef, Fragment} from 'react';
import {Link} from 'wouter';
import { useIntersectionObserver } from 'hocks/useNearScreen'

export const PrintGif = props => {
    const {urlLink, title, urlImg} = props;

    return (<Fragment>
            <img loading='lazy' src={urlImg} alt={title} />
            <Link to={urlLink} target="_blanck">{title}</Link>
        </Fragment>)
}

export const InterPrint = props => {
    const {id} = props, 
        elementRef = useRef(),
        isNearScreens  = useIntersectionObserver({elementRef});

    return (
        <div className="gif-comp" key={id} ref={elementRef}>
            {isNearScreens ?  PrintGif(props) : ''}    
        </div>
    );
}